#include <windows.h>
#include <stdio.h>
#define THREADCOUNT 5
DWORD WINAPI phd(LPVOID lpParameter);
CRITICAL_SECTION chopsticks[5];
int eatCount = 0;
int ind = 0;
int main()
{
    DWORD   dwThreadIdArray[THREADCOUNT];
	HANDLE  hThreadArray[THREADCOUNT];
    for (int i = 0; i < THREADCOUNT; i++)
    {
        InitializeCriticalSection(&chopsticks[i]);
    }
    for (int i = 0; i < THREADCOUNT; i++)
    {
        ind = i;
        hThreadArray[i] = CreateThread(
            NULL,                   // default security attributes
            0,                      // use default stack size  
            phd,     // thread function name
            &i,          // argument to thread function 
            0,                      // use default creation flags 
            &dwThreadIdArray[i]);   // returns the thread identifier 
        if (hThreadArray[i] == NULL)
        {
            printf("CreateThread error: %d\n", GetLastError());
            return 1;
        }
    }


    WaitForMultipleObjects(THREADCOUNT, hThreadArray, TRUE, INFINITE);
    for (int i = 0; i < THREADCOUNT; i++)
        CloseHandle(hThreadArray[i]);
	return 0;
}

DWORD WINAPI phd(LPVOID lpParameter)
{
    eatCount++;

    EnterCriticalSection(&chopsticks[ind]);
    if (ind - 1 == -1)
    {
        EnterCriticalSection(&chopsticks[THREADCOUNT-1]);
    }
    else
        EnterCriticalSection(&chopsticks[ind]);
    if (ind + 1 == THREADCOUNT)
    {
        EnterCriticalSection(&chopsticks[0]);
    }
    else
        EnterCriticalSection(&chopsticks[ind]);

    printf("\n%d is starting eating\n\n", ind);
    for (int i = 0; i < 1000000; i++)
    {
       // printf("Now eating %d\n", i);
    }
    LeaveCriticalSection(&chopsticks[ind]);
    if (ind - 1 == -1)
    {
        LeaveCriticalSection(&chopsticks[THREADCOUNT - 1]);
    }
    else
        LeaveCriticalSection(&chopsticks[ind]);
    if (ind + 1 == THREADCOUNT)
    {
        LeaveCriticalSection(&chopsticks[0]);
    }
    else
        LeaveCriticalSection(&chopsticks[ind]);

    return 0;
}