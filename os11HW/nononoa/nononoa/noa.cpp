#include <windows.h>
#include <stdio.h>
#include <thread>
#include <mutex>
#include <iostream>
#define THREADCOUNT 5
HANDLE mtx[THREADCOUNT]; //forks

std::mutex mtx[THREADCOUNT]; //forks
void main(int argc, int* argv[])
{

    if (argc != 2)
    {
        printf("Usage: %s [cmdline]\n", argv[0]);
        return;
    }
    int philID = *argv[1];
    HANDLE CreateMutexW(NULL,false,mtx);
    int leftIndex = philID - 1;
    int rightIndex = (philID) % (THREADCOUNT - 1);
    mtx[leftIndex].lock();
    mtx[rightIndex].lock();
    mtx[philID].lock();
    const clock_t begin_time = clock();

    for (int i = 0; i < 1000000; i++)
    {

    }
    std::cout << float(clock() - begin_time) / CLOCKS_PER_SEC;
    std::cout << "to " << philID << std::endl;
    mtx[leftIndex].unlock();
    mtx[rightIndex].unlock();
    mtx[philID].unlock();

}